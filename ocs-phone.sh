#!/usr/bin/env bash

# TO-DO:
# remove opt_* variables and use only list_apps, rewrite other function
# flash
# backup        => all data in my phone
# restore       => all data from recovery
# sync          => music, rm don't change list of apps
# update        => get apps from play market

set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT

script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd -P)
 
check_dependency()
{
    if [[ -z ${BASH_USER_LIB-} ]]; then
        local BASH_USER_LIB="$script_dir/lib.sh"
        curl --silent 'https://gitlab.com/bs0c/lib.bash/-/raw/master/lib.sh' --output "$BASH_USER_LIB"
    fi
    source "$BASH_USER_LIB"
}

usage()
{
    cat <<EOF

    Usage: $(basename "${BASH_SOURCE[0]}") [-f {recovery, image, magisk, gapps}] [-s] [-u] [-[bri] {list apps or file}] [-h] [-v]

    Script for install LineageOS and some apps on a phone.

    Available options:

    -h, --help      Print this help and exit
    -v, --verbose   Print script debug info
    --no-color
    -f, --flash     Possible values: recovery, image, magistik, gapps
    -s, --sync      Sync data phone and pc
    -b, --backup    Create backup apps and  their data
    -r, --restore   Restore apps and their data
    -i, --install   Istall apps
    -u, --update    Update apps without market

EOF
exit
}

cleanup()
{
    #trap - SIGINT SIGTERM ERR EXIT
    trap 'msg sigterm' SIGTERM
    trap 'msg exit' EXIT
    trap 'msg ERR' ERR
    trap 'msg sigint' SIGINT
}

parse_params()
{
    local args=("$@")
    android_id="11662af6"
    local_base="$HOME/media/phone"
    adb_sync=

    while :; do
        case "${1-}" in
            -f | --flash)
                is_flash="true"
                opt_flash="${2-}"
                shift
                ;;
            -i | --install)
                is_install="true"
                opt_install="${2-}"
                shift
                ;;
            -b | --backup)
                is_backup="true"
                opt_backup="${2-}"
                shift
                ;;
            -r | --restore)
                is_restore="true"
                opt_restore="${2-}"
                shift
                ;;
            -s | --sync)
                is_sync="true" ;;
            -u | --update)
                is_update="true" ;;

            -s | --source)
                SOURCE="${2-}"
                shift
                ;;
            -l | --list)
                list_apps="${2-}"
                shift
                ;;
            -h | --help) usage ;;
            -v | --verbose) 
                set -x
                PS4='`basename $0` [$LINENO]: '
                ;;
            --no-color) NO_COLOR=1;;
            -?*) die "Unknown option: ${1-}" ;;
            *) 
                break;;
        esac
        shift
    done

# check required params and arguments

    # msg "args"
    # [[ ${#args[@]} -eq 0 ]] && die "Need arguements see './install -h'"
    # msg "fash"
    # [[ -n ${is_flash-} && -z ${opt_flash-} ]] && die "Missing required parameter: ${RED}opt_flash${NOFORMAT}"
    # msg "install"
    # [[ -n ${is_install-} && -z ${opt_install-} ]] && die "Missing required parameter: ${RED}opt_install${NOFORMAT}"
    # msg "backup"
    # [[ -n ${is_backup-} && -z ${opt_backup-} ]] && die "Missing required parameter: ${RED}opt_backup${NOFORMAT}"
    # msg "restore"
    # [[ -n ${is_restore-} && -z ${opt_restore-} ]] && die "Missing required parameter: ${RED}opt_restore${NOFORMAT}"

    return 0
}

check_phone()
{
    msg "[-] check phone...\c"
    adb devices | grep -q "$android_id[[:blank:]]*device" || die "\t\tadb cannot find device"
    msg "${GREEN}\r[+]${NOFORMAT}"
}


flash()
{
    if [[ $opt_flash =~ recover ]]; then
        msg "[-] flash recover...\c"
        adb reboot bootloader
        sleep 10
        fastboot devices | grep -q "$android_id[[:blank:]]*fastboot" || die "fastboot: cannot find device"
        fastboot flash recovery "$path"/lineage-18.1-20230101-recovery-ginkgo.img
        fastboot reboot
        msg "${GREEN}\r[+]${NOFORMAT}"
    fi

    if [[ $opt_flash =~ image ]]; then
        msg "[-] flash image...\c"
        adb reboot recovery
        msg "choose: Factory Reset ⇒ Format data / factory ⇒ Format data"
        read -p "and after reset data press Enter to continue" < /dev/tty
        msg "choose: Apply Update ⇒ Apply from ADB"
        read -p "and then press Enter to continue" < /dev/tty
        adb sideload "$path"/lineage-18.1-20230101-nightly-ginkgo-signed.zip
        msg "${GREEN}\r[+]${NOFORMAT}"
    fi

    if [[ $opt_flash =~ magisk ]]; then
        msg "[-] flash magisk...\c"
        msg "choose: Apply Update ⇒ Apply from ADB"
        read -p "and then press Enter to continue" < /dev/tty
        adb sideload "$path"/magisk-v26.1.zip
        msg "${GREEN}\r[+]${NOFORMAT}"
    fi

    if [[ $opt_flash =~ gapps ]]; then
        msg "[-] flash gapps...\c"
        msg "choose: Apply Update ⇒ Apply from ADB"
        read -p "and then press Enter to continue" < /dev/tty
        adb sideload "$path"/open_gapps-arm64-11.0-nano-20220215.zip
        msg "${GREEN}\r[+]${NOFORMAT}"
    fi

    msg "[-] reboot phone...\c"
    adb reboot
    msg "${GREEN}\r[+]${NOFORMAT}"

}

install()
{
    msg "[-] install apps..."
    local list_app=$opt_install
    local apk_path="$local_base/backup/apps"
    if [[ -f $opt_install ]]; then
        list_app=$(cat $opt_install | sed 's/^#*//')
    fi
    for app in $list_app; do
        msg "${GREEN}\t$app: ${NOFORMAT}\c"
        adb install $apk_path/$app
        msg "${GREEN}\r\t\t\t\t\t\t    ok\t${NOFORMAT}"
    done
    msg "${GREEN}\r[+]${NOFORMAT}"
}

backup()
{
    msg "[-] backup apps..."
    local list_app=$opt_backup
    local backup_path="$local_base/backup/apps"
    if [[ -f $opt_backup ]]; then
        list_app=$(cat $opt_backup | sed 's/^#*//')
    fi
    for app in $list_app; do
        msg "${GREEN}\t$app: ${NOFORMAT}\c"
        adb backup -f $backup_path/$app.db $app &>/dev/null
        adb pull $(adb shell pm path $app | grep "base.apk" | sed 's/^package://') $backup_path/$app &>/dev/null
        msg "${GREEN}\r\t\t\t\t\t\t    ok\t${NOFORMAT}"
    done
    msg "${GREEN}\r[+]${NOFORMAT}"
}

restore()
{
    msg "[-] restore apps..."
    local list_app=$opt_restore
    local path_backup=$local_base/backup/apps
    if [[ -f $opt_restore ]]; then
        list_app=$(cat $opt_restore | sed 's/^#*//')
    fi
    for app in $list_app; do
        msg "${GREEN}\t$app: ${NOFORMAT}\c"
        adb restore $path_backup/$app.db &>/dev/null
        msg "${GREEN}\r\t\t\t\t\t\t    ok\t${NOFORMAT}"
    done
    msg "${GREEN}\r[+]${NOFORMAT}"
}

check_adb_sync()
{
    adb_sync="$script_dir"/better-adb-sync/src/adbsync.py
    [[ -f $adb_sync ]] || git clone https://github.com/jb2170/better-adb-sync.git
}

sync()
{
    check_adb_sync
    msg "[-] sync data..."
    $adb_sync --quiet --copy-links pull /storage/emulated/0/Books "$local_base"/backup
    $adb_sync --quiet --copy-links push "$local_base"/backup/Books /storage/emulated/0/Books
    $adb_sync --quiet --delete pull /storage/emulated/0/DCIM "$local_base"/backup
    $adb_sync --quiet --delete pull /storage/emulated/0/Documents "$local_base"/backup
    $adb_sync --quiet --delete pull /storage/emulated/0/Download "$local_base"/backup
    msg "${GREEN}\r[+]${NOFORMAT}"
    #$adb_sync --show-progress pull "$local_base"/backup/Music /storage/self/primary/Music
    msg "[-] sync list installed apps..."
    adb shell pm list packages -3 | sed 's/^package://' > "$local_base"/installed-apps-$(date +%Y-%m-%d-%H-%M)
    msg "${GREEN}\r[+]${NOFORMAT}"
}

update()
{
    msg "[-] update apps..."
    msg "${GREEN}\r[+]${NOFORMAT}"
}

check_dependency
setup_colors
parse_params "$@"

check_phone

[[ -n ${is_flash-} ]] && flash
[[ -n ${is_install-} ]] && install
[[ -n ${is_backup-} ]] && backup
[[ -n ${is_restore-} ]] && restore
[[ -n ${is_sync-} ]] && sync
[[ -n ${is_update-} ]] && update
